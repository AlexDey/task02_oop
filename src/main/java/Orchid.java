public class Orchid extends FlowerShopItem {
    public Orchid(ItemNames name, double price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    boolean addToBouquet() {
        return true;
    }
}
