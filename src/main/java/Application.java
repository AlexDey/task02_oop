import java.util.ArrayList;
import java.util.Scanner;

public class Application {
    private static ArrayList<FlowerShopItem> basket = new ArrayList<>();
    private static ArrayList<FlowerShopItem> bouquet = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Welcome to out flower shop! \n" +
                "What would you like to add to your basket and how many pieces? \n" +
                "Please, enter two space separated numbers end press Enter. \n" +
                "For instance, 2 5 means you would like to add five chamomiles \n");
        mainMenu();
        initBasket();
    }

    public static void makeBouquet() {
        for (FlowerShopItem item : basket) {
            if (item.addToBouquet()) {
                bouquet.add(item);
            }
        }
        System.out.println("Your bouquet is ready! " +
                "It consists of " + bouquet.size() + " types of flowers: \n" +
                "The total number of flowers is " + totalNumberFlowers());
    }

    private static void mainMenu() {
        System.out.println("1 Rose \n" +
                "2 Chamomile \n" +
                "3 Orchid \n" +
                "4 Flowerpot \n" +
                "5 Make a bouquet \n" +
                "6 Go to the cashier \n" +
                "7 Exit");
        initBasket();
    }

    private static double countTotalPrice() {
        double totalPrice = 0;
        for (FlowerShopItem item : basket) {
            totalPrice = totalPrice + item.price * item.quantity;
        }
        return totalPrice;
    }

    private static int totalNumberFlowers() {
        int totalNumber = 0;
        for (FlowerShopItem item : basket) {
            if (item.addToBouquet()) {
                totalNumber = totalNumber + item.quantity;
            }
        }
        return totalNumber;
    }

    private static void initBasket() {
        String[] order = scanner.nextLine().split(" ");
        if (order.length < 2) {
            if (order[0].equals("5")) {
                makeBouquet();
                mainMenu();
            }
            if (order[0].equals("6")) {
                System.out.println("The total price is " + countTotalPrice() +
                        "\n" + "Bye!");
                System.exit(0);
            }
            if (order[0].equals("7")) {
                System.exit(0);
            } else {
                System.out.println("Error! You entered wrong number. Try again \n");
                mainMenu();
                order = scanner.nextLine().split(" ");
            }
        }

        int optionNumber = Integer.parseInt(order[0]);
        int itemQuantity = Integer.parseInt(order[1]);
        switch (optionNumber) {
            case 1:
                basket.add(new Rose(ItemNames.ROSE, 25.0, itemQuantity));
                System.out.println(itemQuantity + " rose(s) was/were added to your basket \n");
                break;
            case 2:
                basket.add(new Chamomile(ItemNames.CHAMOMILE, 15.0, itemQuantity));
                System.out.println(itemQuantity + " chamomile(s) was/were added to your basket \n");
                break;
            case 3:
                basket.add(new Orchid(ItemNames.ORCHID, 35.0, itemQuantity));
                System.out.println(itemQuantity + " orchid(s) was/were added to your basket \n");
                break;
            case 4:
                basket.add(new Flowerpot(ItemNames.FLOWERPOT, 50.0, itemQuantity));
                System.out.println(itemQuantity + " flowerpot(s) was/were added to your basket \n");
                break;
        }
        mainMenu();
    }
}
