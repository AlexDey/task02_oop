public class Chamomile extends FlowerShopItem {
    public Chamomile(ItemNames name, double price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    boolean addToBouquet() {
        return true;
    }
}
