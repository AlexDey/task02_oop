public class Rose extends FlowerShopItem {
    public Rose(ItemNames name, double price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    boolean addToBouquet() {
        return true;
    }
}
