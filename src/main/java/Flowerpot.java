public class Flowerpot extends FlowerShopItem {
    public Flowerpot(ItemNames name, double price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    boolean addToBouquet() {
        return false;
    }
}
