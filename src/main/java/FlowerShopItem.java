public abstract class FlowerShopItem {
    ItemNames name;
    double price;
    int quantity;

    abstract boolean addToBouquet();
}
